package com.example.videoraw;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_RAW extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video__raw);
		VideoView video = (VideoView) findViewById(R.id.videoFrame);
		video.setVideoURI(Uri.parse("http://commonsware.com/misc/test2.3gp"));
		video.setMediaController(new MediaController(this));
		video.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_video__raw, menu);
		return true;
	}

}
